using FortCodeExercises.Exercise1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FortCodeExercises.Tests
{
    [TestClass]
    [TestCategory(nameof(OldMachine))]
    public class OldMachineTests
    {
        private const string PropertyNameType = "machine.type";
        private const string PropertyNameColor = "machine.color";
        private const string PropertyNameTrimColor = "machine.trimColor";
        private const string PropertyNameIsDark = "machine.isDark";

        [TestCategory("Bulldozer")]
        [TestMethod]
        public void Bulldozer_Should_Be_Built_Properly()
        {
            var bulldozer = new OldMachine();
            bulldozer.type = 0;

            Console.WriteLine(bulldozer.description);

            Assert.AreEqual((int)MachineType.Bulldozer, bulldozer.type, PropertyNameType);
            Assert.AreEqual("red", bulldozer.color, PropertyNameColor);
            Assert.AreEqual(string.Empty, bulldozer.trimColor, PropertyNameTrimColor);
            Assert.IsTrue(bulldozer.isDark(bulldozer.color), PropertyNameIsDark);

            Assert.AreEqual($" {bulldozer.color} {bulldozer.name} [{bulldozer.getMaxSpeed(bulldozer.type, true)}].", bulldozer.description);
        }

        [TestCategory("Crane")]
        [TestMethod]
        public void Crane_Should_Be_Built_Properly()
        {
            var crane = new OldMachine();
            crane.type = 1;

            Console.WriteLine(crane.description);

            Assert.AreEqual((int)MachineType.Crane, crane.type, PropertyNameType);
            Assert.AreEqual("blue", crane.color, PropertyNameColor);
            Assert.AreEqual("black", crane.trimColor, PropertyNameTrimColor);
            Assert.IsTrue(crane.isDark(crane.color), PropertyNameIsDark);

            Assert.AreEqual($" {crane.color} {crane.name} [{crane.getMaxSpeed(crane.type, true)}].", crane.description);
        }

        [TestCategory("Tractor")]
        [TestMethod]
        public void Tractor_Should_Be_Built_Properly()
        {
            var tractor = new OldMachine();
            tractor.type = 2;

            Console.WriteLine(tractor.description);

            Assert.AreEqual((int)MachineType.Tractor, tractor.type, PropertyNameType);
            Assert.AreEqual("green", tractor.color, PropertyNameColor);
            Assert.AreEqual("gold", tractor.trimColor, PropertyNameTrimColor);
            Assert.IsTrue(tractor.isDark(tractor.color), PropertyNameIsDark);
            //hasMax = true

            Assert.AreEqual($" {tractor.color} {tractor.name} [{tractor.getMaxSpeed(tractor.type, true)}].", tractor.description);
        }

        [TestCategory("Truck")]
        [TestMethod]
        public void Truck_Should_Be_Built_Properly()
        {
            var truck = new OldMachine();
            truck.type = 3;

            Console.WriteLine(truck.description);

            Assert.AreEqual((int)MachineType.Truck, truck.type, PropertyNameType);
            Assert.AreEqual("yellow", truck.color, PropertyNameColor);
            Assert.AreEqual("silver", truck.trimColor, PropertyNameTrimColor);
            Assert.IsFalse(truck.isDark(truck.color), PropertyNameIsDark);

            Assert.AreEqual($" {truck.color} {truck.name} [{truck.getMaxSpeed(truck.type, false)}].", truck.description);
        }

        [TestCategory("Car")]
        [TestMethod]
        public void Car_Should_Be_Built_Properly()
        {
            var car = new OldMachine();
            car.type = 4;

            Console.WriteLine(car.description);

            Assert.AreEqual((int)MachineType.Car, car.type, PropertyNameType);
            Assert.AreEqual("brown", car.color, PropertyNameColor);
            Assert.AreEqual(string.Empty, car.trimColor, PropertyNameTrimColor);
            Assert.IsFalse(car.isDark(car.color), PropertyNameIsDark);

            Assert.AreEqual($" {car.color} {car.name} [{car.getMaxSpeed(car.type, false)}].", car.description);
        }
    }
}
