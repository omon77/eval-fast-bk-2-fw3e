﻿using FortCodeExercises.Exercise1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FortCodeExercises.Tests
{
    [TestClass]
    [TestCategory(nameof(Machine))]
    public class MachineTests
    {
        private const string PropertyNameMachineType = "machine.MachineType";
        private const string PropertyNameColor = "machine.Color";
        private const string PropertyNameTrimColor = "machine.TrimColor";

        [TestCategory(nameof(MachineType.Bulldozer))]
        [TestMethod]
        public void Bulldozer_Should_Be_Built_Properly()
        {
            var bulldozer = new Bulldozer();
            Assert.AreEqual(MachineType.Bulldozer, bulldozer.MachineType, PropertyNameMachineType);
            Assert.AreEqual(BaseColor.Red, bulldozer.Color, PropertyNameColor);
            Assert.AreEqual(TrimColor.NotSet, bulldozer.TrimColor, PropertyNameTrimColor);

            Assert.AreEqual(ExpectedDescription(bulldozer), bulldozer.Description);
            Console.WriteLine(bulldozer.Description);
        }

        [TestCategory(nameof(MachineType.Crane))]
        [TestMethod]
        public void Crane_Should_Be_Built_Properly()
        {
            var crane = new Crane();

            Assert.AreEqual(MachineType.Crane, crane.MachineType, PropertyNameMachineType);
            Assert.AreEqual(BaseColor.Blue, crane.Color, PropertyNameColor);
            Assert.AreEqual(TrimColor.Black, crane.TrimColor, PropertyNameTrimColor);

            Assert.AreEqual(ExpectedDescription(crane), crane.Description);
            Console.WriteLine(crane.Description);
        }

        [TestCategory(nameof(MachineType.Tractor))]
        [TestMethod]
        public void Tractor_Should_Be_Built_Properly()
        {
            var tractor = new Tractor();

            Assert.AreEqual(MachineType.Tractor, tractor.MachineType, PropertyNameMachineType);
            Assert.AreEqual(BaseColor.Green, tractor.Color, PropertyNameColor);
            Assert.AreEqual(TrimColor.Gold, tractor.TrimColor, PropertyNameTrimColor);

            Assert.AreEqual(ExpectedDescription(tractor), tractor.Description);
            Console.WriteLine(tractor.Description);
        }

        [TestCategory(nameof(MachineType.Truck))]
        [TestMethod]
        public void Truck_Should_Be_Built_Properly()
        {
            var truck = new Truck();

            Assert.AreEqual(MachineType.Truck, truck.MachineType, PropertyNameMachineType);
            Assert.AreEqual(BaseColor.Yellow, truck.Color, PropertyNameColor);
            Assert.AreEqual(TrimColor.NotSet, truck.TrimColor, PropertyNameTrimColor);

            Assert.AreEqual(ExpectedDescription(truck), truck.Description);
            Console.WriteLine(truck.Description);
        }

        [TestCategory(nameof(MachineType.Car))]
        [TestMethod]
        public void Car_Should_Be_Built_Properly()
        {
            var car = new Car();

            Assert.AreEqual(MachineType.Car, car.MachineType, PropertyNameMachineType);
            Assert.AreEqual(BaseColor.Brown, car.Color, PropertyNameColor);
            Assert.AreEqual(TrimColor.NotSet, car.TrimColor, PropertyNameTrimColor);

            Assert.AreEqual(ExpectedDescription(car), car.Description);
            Console.WriteLine(car.Description);
        }

        private string ExpectedDescription(Machine machine)
            => $" {machine.Color.ToString().ToLowerInvariant()} {machine.Name.ToLowerInvariant()} [{machine.MaxSpeed.GetValueOrDefault()}].";
    }
}