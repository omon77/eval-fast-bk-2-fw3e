﻿namespace FortCodeExercises.Exercise1
{
    public enum BaseColor
    {
        NotSet = -1,
        White = 0,
        Blue,
        Brown,
        Green,
        Red,
        Yellow
    }
}