﻿namespace FortCodeExercises.Exercise1
{
    public enum TrimColor
    {
        NotSet = -1,
        White,
        Black,
        Gold,
        Silver
    }
}