﻿namespace FortCodeExercises.Exercise1
{
    public enum MachineType
    {
        NotSet = -1,
        Bulldozer = 0,
        Crane = 1,
        Tractor = 2,
        Truck = 3,
        Car = 4
    }
}