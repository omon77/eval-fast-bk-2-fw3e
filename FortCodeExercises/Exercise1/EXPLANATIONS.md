﻿# Explanations

## Summary

From the looks of the original code, the intent was to be able to set the class-level field `type` on the object, 
and based on that value certain specifications/traits/behaviors would be exhibited by the object.

This implementation attempted to keep the state of the object intact, by providing only get accessors.

By exposing the `machineName` and `type` as public fields, at run time, this can lead to adverse side-effects of the object
inadvertently being morphed into another type of machinery, exhibiting incorrect behavior and traits (i.e. a car turning into a bulldozer).

-----

### Actual refactoring applied
* Test coverage of the 'as-is' class.
* Refactored to use OOP and SOLID design principles.
* Code style adheres to Microsoft naming conventions.
* Removed the instances of method-scoped variables that were hiding identically-named class-level fields.
* Made code less brittle (removal of all of the convoluted if/else).
* Easier to debug/maintain/evolve.
* Major defects identified/fixed:
  * `getMaxSpeed()` method's second argument `noMax` has opposite meaning of the way it is being used (`hasMaxSpeed`).
  * `isDark()` method returns *`false`* for color *`blue`*.
  * `isDark()` method returns *`false`* for color *`brown`*.
  * `trimColor()` method returns _`white`_ for _`crane`_ instead of _`black`_.
  * `trimColor()` method returns empty string for Truck instead of _`silver`_.