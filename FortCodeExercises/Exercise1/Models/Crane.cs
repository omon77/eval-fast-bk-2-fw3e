﻿namespace FortCodeExercises.Exercise1
{
    public class Crane : Machine
    {
        private readonly TrimColor _trimColor = TrimColor.Black;

        public Crane() : base(MachineType.Crane)
        { }

        public override BaseColor Color => BaseColor.Blue;
        public override int? MaxSpeed => 75;
        public override TrimColor TrimColor => _trimColor;
    }
}