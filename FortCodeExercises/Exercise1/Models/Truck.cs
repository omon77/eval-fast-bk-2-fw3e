﻿namespace FortCodeExercises.Exercise1
{
    public class Truck : Machine
    {
        private readonly TrimColor _trimColor = TrimColor.NotSet;

        public Truck() : base(MachineType.Truck)
        { }

        public override BaseColor Color => BaseColor.Yellow;
        public override int? MaxSpeed => 70;
        public override TrimColor TrimColor => _trimColor;
    }
}