﻿namespace FortCodeExercises.Exercise1
{
    public class Car : Machine
    {
        private readonly TrimColor _trimColor = TrimColor.NotSet;

        public Car() : base(MachineType.Car)
        { }

        public override BaseColor Color => BaseColor.Brown;
        public override int? MaxSpeed => 70;
        public override TrimColor TrimColor => _trimColor;
    }
}