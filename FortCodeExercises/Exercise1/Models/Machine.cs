﻿using System;

namespace FortCodeExercises.Exercise1
{
    public abstract class Machine
    {
        private readonly MachineType _machineType;

        protected Machine(MachineType machineType)
        {
            _machineType = machineType;
        }

        public abstract BaseColor Color { get; }
        public abstract Nullable<int> MaxSpeed { get; }
        public abstract TrimColor TrimColor { get; }

        public string Description => $" {NormalizeAsString(Color)} {Name} [{MaxSpeed}].";
        public bool HasMaxSpeed => MaxSpeed.GetValueOrDefault() > 0;
        public MachineType MachineType => _machineType;
        public string Name => NormalizeAsString(MachineType);

        public static string NormalizeAsString(object value) => value.ToString().ToLowerInvariant();
    }
}