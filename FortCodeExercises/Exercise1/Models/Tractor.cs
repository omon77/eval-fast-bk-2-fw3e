﻿namespace FortCodeExercises.Exercise1
{
    public class Tractor : Machine
    {
        private readonly TrimColor _trimColor = TrimColor.Gold;

        public Tractor() : base(MachineType.Tractor)
        { }

        public override BaseColor Color => BaseColor.Green;
        public override int? MaxSpeed => 90;
        public override TrimColor TrimColor => _trimColor;
    }
}