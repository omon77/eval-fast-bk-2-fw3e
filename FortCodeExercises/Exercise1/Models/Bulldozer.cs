﻿namespace FortCodeExercises.Exercise1
{
    public class Bulldozer : Machine
    {
        private readonly TrimColor _trimColor = TrimColor.NotSet;

        public Bulldozer() : base(MachineType.Bulldozer)
        { }

        public override BaseColor Color => BaseColor.Red;
        public override int? MaxSpeed => 80;
        public override TrimColor TrimColor => _trimColor;
    }
}